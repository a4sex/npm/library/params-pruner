import prune from '../dist/index.js';

describe('Проверить', () => {
  let object;
  beforeEach(() => {
    object = {
      value: 1,
      empty: '',
      null: null,
      nan: NaN,
      defined: undefined,
      boolean: false,
      inline: {
        value: 1,
        empty: '',
        null: null,
        nan: NaN,
        defined: undefined,
        boolean: false,
      }
    };
  });

  it('удаление', async () => {
    const result = prune(object);
    const expected = { 
      value: 1, 
      boolean: false,
      inline: { 
        value: 1,
        boolean: false,
      } 
    };

    expect(result).toEqual(expected);
  });

});
