# Params pruner

A lightweight NPM package that provides a single function, prune, which removes all empty values from an object, including empty strings, null, NaN, and undefined, across all nested levels.Additionally, it preserves boolean fields with a value of false in the pruned object.

This means that boolean fields with a value of false will not be considered empty and will be retained in the pruned object. Only values that fall under the empty criteria (empty strings, null, NaN, and undefined) will be removed.

## Usage

To use the prune function, import it into your project. The prune function takes a single parameter, which should be an object. It removes all empty values from the object recursively and returns the pruned object.

```js

import prune from '@a4sex/params-pruner';

let object = {
  value: 1,
  empty: '',
  null: null,
  nan: NaN,
  defined: undefined,
  boolean: false,
  inline: {
    value: 1,
    empty: '',
    null: null,
    nan: NaN,
    defined: undefined,
    boolean: false,
  }
};

const result = prune(object);

console.log(result);
// Output: { 
//   value: 1, 
//   boolean: false,
//   inline: { 
//     value: 1 
//     boolean: false,
//   } 
// }

```
