import { mapObject, isObject, isEmpty, pick, isNull, isNaN, isUndefined } from 'underscore';
export default function (params) {
    function prune(object) {
        object = mapObject(object, value => {
            if (isObject(value)) {
                if (isEmpty(value)) {
                    return null;
                }
                return prune(value);
            }
            return value;
        });
        return pick(object, value => value !== '' && (!isObject(value) || !isEmpty(value)) && !isNull(value) && !isNaN(value) && !isUndefined(value));
    }
    return prune(params);
}
;
